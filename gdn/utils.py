import re
import textwrap


def sorted_nicely(list_of_questions):
    """ Sorts the given iterable in the way that is expected.

    Required arguments:
    l -- The iterable to be sorted.

    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda q: [convert(c) for c in re.split('([0-9]+)', q.qid)]

    return sorted(list_of_questions, key=alphanum_key)


def wrap_text(text, width=200):
    wrapped = textwrap.wrap(text, width=width)
    if len(wrapped) <= 2:
        return text
    else:
        return wrapped[0] + '[...]' + wrapped[-1]
