import collections

from gdn import INCLASSABLES, NON_REPONSES
import gdn.utils


def compare_methods_results(question, clusters):
    methods = clusters.keys()
    md = '**Question {}** : {}  \n\n'.format(question.qid, question.text)
    md += ('|{:60.60}|{:8}|' + '{:8}|' * len(methods)).format('Catégorie', 'OW', *methods) + '  \n'
    md += ('|----|----|' + '----|' * len(methods)) + '  \n'

    result_per_categories = {m: clusters[m].get_nb_responses_per_categories() / question.nb_total_responses for m in
                             methods}

    for c in question.categories.values():
        label = c.id + ' ' + c.text

        fmt_str = '{:.2%}'
        md += (('|{}|' + fmt_str + '|{}|  \n').format(label,
                                                      question.gold[c.id] / 100,
                                                      '|'.join([fmt_str.format(result_per_categories[m][c.id])
                                                                if c.id in result_per_categories[m] else 'NA'
                                                                for m in methods])))
    md += ('|{}|' + fmt_str + '|{}|  \n').format(INCLASSABLES,
                                                 question.gold[INCLASSABLES] / 100,
                                                 '|'.join([fmt_str.format(
                                                     clusters[m].nb_inclassables() / question.nb_total_responses)
                                                           for m in methods]))

    md += ('|{}|' + fmt_str + '|{}|  \n').format(NON_REPONSES,
                                                 question.gold[NON_REPONSES] / 100,
                                                 '|'.join([fmt_str.format(
                                                     1 - len(clusters[m].matrix) / question.nb_total_responses)
                                                           for m in methods]))

    return md

def compare_methods_response(question, response_id, clusters):
    if response_id not in question.responses:
        return 'Réponse vide'
    methods = list(clusters.keys())

    md = '**Question {}** : {}  \n'.format(question.qid, question.text)
    md += '**Réponse {}** : {}  \n'.format(response_id, question.responses[response_id])

    md += '\n'
    md += ('|{:60.60}|' + '{:10}|' * len(methods)).format('Catégorie', *methods) + '  \n'
    md += '|----|' + '----|' * len(methods) + '  \n'

    for category in question.categories.values():
        label = '**' + category.id + ' ' + category.text + '**'
        if category.subcategories:
            label += '<br/>*('
            label += ','.join(category.subcategories)
            label += ')*'
        md += ('|{}|' + '{:^10}|' * len(methods) + '  \n').format(label,
                                                                  *['x' if clusters[m].are_associated(response_id,
                                                                                                      category.id) else '<i></i>'
                                                                    for m in methods])

    return md


def cluster_overview(question, category_id, distances, threshold, N=20):
    """Generate a markdown report with an overview of the responses associated with a category

    """

    category = question.categories[category_id]

    md = "### Réponses associées à une catégorie  \n"
    md += "**Question:** " + question.text + "  \n"

    md += "**Catégorie:** " + category.text + "  \n"
    md += "**Sous-catégories:** " + '; '.join(category.subcategories) + "  \n"

    if isinstance(threshold, dict):
        threshold = threshold[category_id]
    sorted_distances = distances[category_id].sort_values()
    nb = len(sorted_distances[sorted_distances < threshold])

    md += "\n**Réponses triées par ordre croissant, des plus proches de la catégorie aux plus éloignées:**  \n"
    md += '\n'

    md_row = "| {position} | {distance} | {rid} | {text} |  \n"
    md_sep = md_row.format(position=".......", distance='........', rid='..........', text='............')

    md += md_row.format(position="Position", distance='Distance', rid='id  ', text='text')
    md += md_row.format(position="--------", distance='--------', rid='----', text='----')

    for i in range(N):
        position = i
        r_id = sorted_distances.index[position]
        r = question.responses[r_id]
        md += md_row.format(position=position,
                            distance='{:.5f}'.format(sorted_distances[r_id]),
                            rid=r_id,
                            text=gdn.utils.wrap_text(r))

    md += md_sep
    for i, r_id in enumerate(sorted_distances.index[nb-N:nb],
                             start=nb - N):
        r = question.responses[r_id]
        md += md_row.format(position=i,
                            distance=sorted_distances[r_id],
                            rid=r_id,
                            text=gdn.utils.wrap_text(r))

    md += "\n**Réponses au delà du seuil, triées par ordre croissant, des plus proches de la catégorie aux plus éloignées:**  \n"
    md += '\n'

    md_row = "| {position} | {distance} | {rid} | {text} |  \n"

    md += md_row.format(position="Position", distance='Distance', rid='id  ', text='text')
    md += md_row.format(position="--------", distance='--------', rid='----', text='----')

    for i in range(N):
        position = i
        r_id = sorted_distances.index[nb + position]
        r = question.responses[r_id]
        md += md_row.format(position=nb + position,
                            distance='{:.5f}'.format(sorted_distances[r_id]),
                            rid=r_id,
                            text=gdn.utils.wrap_text(r))

    return md


def inclassables_overview(question, distances, threshold, N=20):
    md = "### Réponses inclassables (triées par ordre croissant de leur distance à une catégorie)  \n"
    md += "**Question:** " + question.text + "  \n"
    md += "**Catégories:** " + ';'.join([c.text for c in question.categories.values()]) + "  \n"

    thresholded_distances = distances < threshold
    inclassables = thresholded_distances[~thresholded_distances.any(axis=1)].index
    min_distances = distances.loc[inclassables].min(axis=1)
    sorted_distances = min_distances.sort_values()

    N = min(N, len(inclassables))

    md += "\n**Réponses triées par ordre croissant, des plus proches de la catégorie aux plus éloignées:**  \n"
    md += '\n'

    md_row = "| {position} | {distance} | {rid} | {text} |  \n"
    md_sep = md_row.format(position=".......", distance='........', rid='..........', text='............')

    md += md_row.format(position="Position", distance='Distance', rid='id  ', text='text')
    md += md_row.format(position="--------", distance='--------', rid='----', text='----')

    for i in range(N):
        position = i
        r_id = sorted_distances.index[position]
        r = question.responses[r_id]
        md += md_row.format(position=position,
                            distance='{:.5f}'.format(min_distances[r_id]),
                            rid=r_id,
                            text=gdn.utils.wrap_text(r))

    if len(inclassables) > N:
        md += md_sep

        for i, r_id in enumerate(sorted_distances.index[-N:],
                                 start=len(min_distances) - N):
            r = question.responses[r_id]
            md += md_row.format(position=i,
                                distance=min_distances[r_id],
                                rid=r_id,
                                text=gdn.utils.wrap_text(r))
    return md


def search_responses_result(question, pattern, max_reponses_to_display=10):
    search_result = question.search_in_responses(pattern)
    md = ''
    if len(search_result) < max_reponses_to_display:
        md += '|{}|{}|  \n'.format('id', 'text')
        md += '|----|----|  \n'
        md += '  \n'.join(['|{}|{}|'.format(r, question.responses[r]) for r in search_result])
        md += '\n'
    else:
        reponses_counter = collections.Counter([question.responses[r] for r in search_result])
        displayed = reponses_counter.most_common(max_reponses_to_display)

        if len(reponses_counter) > max_reponses_to_display:
            md += "Trop de réponses à afficher ({}) : ".format(len(reponses_counter))
            md += "Seuls les {} libellés des réponses les plus fréquents, ".format(max_reponses_to_display)
            md += "ce qui représente {} réponses sur les {}.  \n\n".format(sum([n for _, n in displayed]),
                                                                           sum(reponses_counter.values()))
            md += '  \n\n'

        md += '|{}|{}|  \n'.format('nb', 'text')
        md += '|----|----|  \n'
        md += '  \n'.join(['|{}|{}|'.format(n, t)
                           for t, n in displayed])
        md += '\n'
        if len(reponses_counter) > max_reponses_to_display:
            md += '| ... | ...|  \n\n'
        md += "Total = {} libellés pour {} réponses   \n".format(len(reponses_counter), sum(reponses_counter.values()))
    return md
