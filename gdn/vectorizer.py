import numpy as np
import pandas as pd

import sklearn.feature_extraction

from gdn.data import Question
from gdn import INCLASSABLES


class Vectorizer:
    def __init__(self, question):
        self.question = question
        self.distances_ = None
        self.thresholds_ = None

    def fit(self):
        pass

    def get_contributions(self, reponse_id, categorie_id):
        pass

    def get_distances(self):
        if self.distances_ is None:
            cosine_distances = sklearn.metrics.pairwise_distances(self.responses_as_vectors_,
                                                                  self.categories_as_vectors_, metric='cosine')
            self.distances_ = pd.DataFrame(cosine_distances,
                                           columns=self.categories_as_vectors_.index,
                                           index=self.responses_as_vectors_.index)
        return self.distances_

    def get_thresholds(self):
        if self.thresholds_ is None:
            self.thresholds_ = {'gold': self.get_thresholds_from_gold(),
                                'inclassables': self.get_threshold_from_inclassables(),
                                'total': self.get_threshold_from_total()}
        return self.thresholds_

    def get_thresholds_from_gold(self):
        thresholds = {}
        for category in self.question.categories.values():
            sorted_distances = sorted(self.distances_[category.id])
            N = int(self.question.gold[category.id] * self.question.nb_total_responses / 100)
            # N is the number of responses that should be associated to this category

            # but we add all the responses at the same distance than the last one
            while N < len(sorted_distances) - 1 and sorted_distances[N - 1] == sorted_distances[N]:
                N += 1
            thresholds[category.id] = (sorted_distances[N - 1] + sorted_distances[N]) / 2
        return thresholds

    def get_threshold_from_inclassables(self):
        inclassables = float(self.question.gold[INCLASSABLES]) if INCLASSABLES in self.question.gold else 0
        N = int(self.question.nb_total_responses * inclassables / 100)
        # N is the number of responses that should be unclassifiable, i.e. too far of all categories to be classified
        if N == 0:
            # all responses are classifiable in categories
            return self.distances_.max().max() + 1
        if N >= len(self.distances_):
            # all responses are unclassifiable
            return 0

        min_distances = sorted(np.min(self.distances_, axis=1), reverse=True)
        while N < len(min_distances) - 1 and min_distances[N - 1] == min_distances[N]:
            N += 1

        return (min_distances[N - 1] + min_distances[N]) / 2

    def get_threshold_from_total(self):
        tx_total = sum([self.question.gold[c] / 100 for c in self.question.categories])
        N = int(self.question.nb_total_responses * tx_total)
        # N is the number of associations between responses and categories that should be predicted

        sorted_distances = sorted(self.distances_.values.flatten())
        while N < len(sorted_distances) - 1 and sorted_distances[N - 1] == sorted_distances[N]:
            N += 1

        return (sorted_distances[N - 1] + sorted_distances[N]) / 2


class TfIdfVectorizer(Vectorizer):
    """Class to vectorize responses and categories of a question using TF-IDF
    """
    def __init__(self, question: Question, stopwords):
        super(TfIdfVectorizer, self).__init__(question)
        self.stopwords = stopwords

    def fit(self):
        # to vectorize a category, we use the text of the category itself and the labels of its subcategories
        categories_labels = {c.id: c.text + ' ' + ' '.join(c.subcategories) for c in self.question.categories.values()}

        sorted_categories_ids = list(categories_labels.keys())
        sorted_reponses_ids = list(self.question.responses.keys())

        corpus = [categories_labels[c] for c in sorted_categories_ids] + [self.question.responses[r_id]
                                                                   for r_id in sorted_reponses_ids]
        self.vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(stop_words=self.stopwords, tokenizer=None)

        vectors = self.vectorizer.fit_transform(corpus)
        empty_docs_indices = np.array(vectors.sum(axis=1) == 0).ravel()

        k = len(categories_labels)
        ignored_categories = []

        for i in range(k):
            if empty_docs_indices[i]:
                print("!!! catégorie OOV:", corpus[i], categories_labels[sorted_categories_ids[i]])
                ignored_categories.append(i)
        for i in ignored_categories:
            del sorted_categories_ids[i]

        valid_responses = [sorted_reponses_ids[i]
                           for i, invalid in enumerate(empty_docs_indices[k:]) if not invalid]
        self.invalid_responses = [sorted_reponses_ids[i]
                                  for i, invalid in enumerate(empty_docs_indices[k:]) if invalid]

        vectors = vectors[~empty_docs_indices].A

        self.categories_as_vectors_ = pd.DataFrame(vectors[:len(sorted_categories_ids)], index=sorted_categories_ids)
        self.responses_as_vectors_ = pd.DataFrame(vectors[len(sorted_categories_ids):], index=valid_responses)

    def _vectorize(self, document):
        return self.vectorizer.transform([document]).A[0]


class WEVectorizer(Vectorizer):
    """Vectorize a document as the mean of the vectors of the words in the document """

    def __init__(self, question, words_vectors, stopwords):
        super(WEVectorizer, self).__init__(question)
        self.words_vectors = words_vectors
        self.stopwords = stopwords

    def fit(self):
        categories_labels = {c.id: c.text + ' ' + ' '.join(c.subcategories) for c in self.question.categories.values()}

        # vectorize categories
        valid_categories = []
        vectors = []

        for cat_id in categories_labels:
            v_cat = self._vectorize(categories_labels[cat_id])
            if v_cat is not None:
                valid_categories.append(cat_id)
                vectors.append(v_cat)
            else:
                print("!!! catégorie OOV:", categories_labels[cat_id])

        self.categories_as_vectors_ = pd.DataFrame(np.array(vectors), index=valid_categories)

        # vectorize responses
        valid_responses = []
        vectors = []

        for response_id in self.question.responses:
            r = self._vectorize(self.question.responses[response_id])
            if r is not None:
                valid_responses.append(response_id)
                vectors.append(r)

        self.responses_as_vectors_ = pd.DataFrame(np.array(vectors), index=valid_responses)

    def _vectorize(self, document):
        X = np.array([self.words_vectors[w] for w in document.split()
                      if w not in self.stopwords and w in self.words_vectors.words])
        if X.shape[0] == 0:
            return None
        return X.mean(axis=0)


class WWEVectorizer(Vectorizer):
    """Vectorize a document as the mean of the vectors of the words in the document weighted with TF-IDF coefficients"""

    def __init__(self, question, words_vectors, stopwords):
        super(WWEVectorizer, self).__init__(question)
        self.words_vectors = words_vectors
        self.stopwords = stopwords

    def fit(self):
        categories_labels = {c.id: c.text + ' ' + ' '.join(c.subcategories) for c in self.question.categories.values()}

        # keep track of the order of categories and responses
        categories = list(self.question.categories.keys())
        responses = list(self.question.responses.keys())

        corpus = [categories_labels[cid] for cid in categories] + [self.question.responses[rid] for rid in responses]
        vocabulary = list({w for d in corpus for w in d.split()
                           if w in self.words_vectors.words and w not in self.stopwords})
        reduced_matrix = np.array([self.words_vectors[w] for w in vocabulary])

        self.tfidf_vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(vocabulary=vocabulary)
        weights = self.tfidf_vectorizer.fit_transform(corpus)

        vectors = weights.dot(reduced_matrix)

        categories_vectors = vectors[:len(categories)]
        valid_categories_indices = np.where(categories_vectors.any(axis=1))[0]

        categories_vectors = categories_vectors[valid_categories_indices]
        categories_weights = weights[:len(categories)][valid_categories_indices].A

        self.categories_as_vectors_ = pd.DataFrame(categories_vectors / categories_weights.sum(axis=1)[:, np.newaxis],
                                                   index=[categories[i] for i in valid_categories_indices])

        responses_vectors = vectors[len(categories):]
        valid_responses_indices = np.where(responses_vectors.any(axis=1))[0]

        responses_vectors = responses_vectors[valid_responses_indices]
        responses_weights = weights[len(categories_labels):][valid_responses_indices].A

        self.responses_as_vectors_ = pd.DataFrame(responses_vectors / responses_weights.sum(axis=1)[:, np.newaxis],
                                                  index=[responses[i] for i in valid_responses_indices])

    def _vectorize(self, document):
        weights = self.tfidf_vectorizer.transform([document])
        result = 0
        for i, j in zip(*weights.nonzero()):
            word = self.tfidf_vectorizer.vocabulary[j]
            word_weight = weights[i, j]
            word_vector = self.words_vectors[word]
            result += word_weight * word_vector
        return result / weights.sum()
