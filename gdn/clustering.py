import itertools

import numpy as np
import pandas as pd


class Clusters:
    def __init__(self, matrix: pd.DataFrame):
        self.matrix = matrix

    def get_categories(self, response_id):
        associations = self.matrix.loc[response_id]
        return set(associations[associations > 0].index)

    def are_associated(self, response_id, category_id):
        if response_id not in self.matrix.index:
            return False
        return self.matrix.loc[response_id, category_id]

    def get_nb_responses_per_categories(self, category_id=None):
        if category_id:
            m = self.matrix[category_id]
        else:
            m = self.matrix
        return m.sum()

    def nb_inclassables(self):
        return len(self.matrix[~self.matrix.any(axis=1)].index)

    @property
    def inclassables(self):
        return list(self.matrix[~self.matrix.any(axis=1)].index)

    def compare_with_ow(self):
        pass

    @staticmethod
    def create_clusters(distances, thresholds):
        return Clusters(distances < thresholds)

    @staticmethod
    def agreement_scores(clusters: dict, flatten=True):
        # TODO: this implementation has sense only to compare clusters where each categories has the same
        # number of items i.e built on gold scores
        # (i.e. using threshold = gold)
        scores = {}
        for method1, method2 in itertools.combinations(clusters.keys(), 2):
            clusters_from_method1 = clusters[method1]
            clusters_from_method2 = clusters[method2]
            common_responses = [r for r in clusters_from_method1.matrix.index if r in clusters_from_method2.matrix.index]

            common_clusters1 = clusters_from_method1.matrix.loc[common_responses, :]
            common_clusters2 = clusters_from_method2.matrix.loc[common_responses, :]

            scores[(method1, method2)] = {}
            for c in clusters_from_method1.matrix.columns:
                n = sum(common_clusters1.loc[:, c])
                if n == 0:
                    scores[(method1, method2)][c] = 0
                else:
                    scores[(method1, method2)][c] = sum(common_clusters1.loc[:, c] & common_clusters2.loc[:, c]) / n

        if flatten:
            return {(m1, m2): np.mean(list(scores[(m1, m2)].values())) for m1, m2 in scores}
        else:
            return scores
