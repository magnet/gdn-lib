import re
import sys
import csv
csv.field_size_limit(sys.maxsize)

import glob
import json
import os

from gdn.utils import sorted_nicely

import collections

Category = collections.namedtuple('Category', 'id text subcategories')

class Question:
    """Class to store and access to the data related to a question of a theme.

    Attributes
    ----------
    qid: str
        Identifier for a question as in the official report
    text: str
        Label of the question
    categories: dict of Category
        The categories that have been defined in the official report to classify the responses to this question
    responses: dict of str
        The non empty responses extracted from the json file
    nb_empty_responses: int
        Nb of contributions where the answer to this question was an empty string in the JSON file
    nb_predefined_responses: int
        For semi-opened questions, nb of contributions where the answer to this question was
        on of the predetermined value in the JSON file
    gold: dict
        The results extracted from the official report
    """
    def __init__(self, qid, text, categories, responses,
                 nb_empty_responses, nb_predefined_responses, gold):
        self.qid = qid
        self.text = text

        self.categories = categories
        self.responses = responses if responses else {}

        self.nb_empty_responses = int(nb_empty_responses) if nb_empty_responses is not None else None
        self.nb_predefined_responses = int(nb_predefined_responses) if nb_predefined_responses is not None else 0
        self.gold = gold

    @property
    def nb_total_responses(self):
        return self.nb_empty_responses + len(self.responses) + self.nb_predefined_responses

    @property
    def nb_categories(self):
        return len(self.categories)

    @property
    def nb_subcategories(self):
        return sum([len(c.subcategories) for c in self.categories.values()])

    def search_in_responses(self, pattern):
        p = re.compile(pattern)
        return [r for r in self.responses if p.search(self.responses[r])]


class Theme:
    def __init__(self, name, questions, nb_contributions):
        self.name = name
        self.questions = sorted_nicely(questions)
        self.nb_contributions = nb_contributions

    @property
    def nb_questions(self):
        return len(self.questions)


def load_dataset(path, progressbar=None):
    """Load the dataset in a dict where keys are themes names and keys are Theme objects"""
    if progressbar is None:
        progressbar = lambda x: x

    data = {}

    themes = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path, f))]
    for name in themes:
        if os.path.isdir(os.path.join(path, name)):
            print("Loading " + name + ' ...')
            with open(os.path.join(path, name, 'nb_contributions.txt')) as f:
                nb_contributions = int(f.read())

            questions = []
            for question_path in progressbar(glob.glob(os.path.join(path, name) + "/question_*")):
                questions.append(load_question(question_path))

            data[name] = Theme(name, questions, nb_contributions)

    return data


def load_question(question_path):
    with open(question_path + '/categories.csv') as f:
        categories = {row['id']: Category(row['id'],
                                          row['text'],
                                          row['subcategories'].split('|') if row['subcategories'] else [])
                      for row in csv.DictReader(f, delimiter=';')}
    with open(question_path + '/reponses.csv') as f:
        responses = {int(row['position']): row['text'] for row in csv.DictReader(f, delimiter=';')}
    with open(question_path + '/info.json') as f:
        question = Question(**json.load(f),
                            categories=categories,
                            responses=responses)
    return question
