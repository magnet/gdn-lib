Télécharger le dataset et les modèles utilisés :
```
download_models.sh
``` 

Le script `metsem_expes.py` calcule et stocke, pour chaque question, les distances entre les réponses et
les catégories en utilisant différentes méthodes de vectorisation.  
Possibilité de télécharger une archive avec les distances déjà calculées :

http://chercheurs.lille.inria.fr/magnet/GDN/GDN_distances.tar.gz

(extraire et placer dans `output`)


A partir de ces distances, le script exporte différents indicateurs pour comparer les résultats obtenus avec ces méthodes

- avec ceux de la synthèses
- entre les différentes méthodes  

Un notebook est fourni en exemple pour analyser les clusters créés pour une question spécifique. 

