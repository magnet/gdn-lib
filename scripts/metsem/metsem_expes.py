import os
import sys
module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

import csv
import json

import string

import mangoes
import matplotlib.pyplot as plt
import nltk
import tqdm

from gdn import TOTAL, NON_REPONSES
import gdn.data
import gdn.clustering
from gdn.vectorizer import *

threshold = 'inclassables'

PATH_TO_PREPROCESSED = '../GDN_open_questions_data'
themes = {
    'LA_TRANSITION_ECOLOGIQUE': 'TE',
    'ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS': 'OSP',
    'LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES': 'FDP',
    'DEMOCRATIE_ET_CITOYENNETE': 'DC'
}

PATH_TO_DISTANCES = 'output/GDN_distances'

custom_stopwords = [
    'les', "l'", "d'",
    'cela', 'ceci', 'ça', "c'",
    "qu'", "n'", 'si',
    'ils', 'elles',
    'donc',
    'être',
    '...', 'etc'
]

nltk.download('stopwords')
stop_words = nltk.corpus.stopwords.words('french') + list(string.punctuation) + custom_stopwords

methods = ['tfidf', 'we_mngs2', 'we_fasttext', 'wwe_mngs2', 'wwe_fasttext']

path_to_models = {
    'mngs2': './models/ppmi_svd_fr_dim500_win2_shift1_cds0.75_eig0/',
    'fasttext': './models/fasttext_fr_300/'
}
loaded_models = {}


def main():
    # Chargement des données du dataset
    print("Chargement des données")
    data = gdn.data.load_dataset(PATH_TO_PREPROCESSED)

    # Chargement ou calcul des distances, thresholds et clusterings associés
    print("Chargement des distances")
    distances, thresholds = get_distances_and_thresholds(data)

    # EXP 01: comparaison de chacune des méthodes avec les résultats de la synthèse
    # Dans un premier temps, on crée des clusters en fixant le seuil de distances par question
    # de façon à obtenir le même nombre d'inclassables que dans la synthèse
    # On compare les résultats obtenus (en pourcentage) sur chaque catégorie
    # et on calcule l'écart global au niveau des questions en moyennant les écrats de chaque catégorie
    # Les résultats sont exportés dans 2 csv : un par question et un par catégorie
    with open('output/01_stats_questions.csv', mode='w') as fq, open('output/01_stats_categories.csv', mode='w') as fc:
        csv_questions = csv.writer(fq, delimiter=';')
        headers_row = ['theme', 'qid', 'text', 'nb_categories', 'nb_subcategories',
                       'nb_responses__actual', 'nb_responses__gold',
                       'nb_empty_responses', 'empty_responses_ratio__actual',
                       'no_responses_ratio__gold',
                       'responses_length_avg', 'responses_length_std', 'responses_length_median']
        headers_row.extend(['ecart_{}'.format(m) for m in methods])
        csv_questions.writerow(headers_row)

        csv_categories = csv.writer(fc, delimiter=';')
        headers_row = ['theme', 'qid', 'cid', 'text', 'nb_subcategories']
        headers_row.extend(['gold'] + methods)
        csv_categories.writerow(headers_row)

        for theme in data:
            for question in data[theme].questions:
                clusters = {m: gdn.clustering.Clusters.create_clusters(distances[theme][question.qid][m],
                                                                       thresholds[theme][question.qid][m]['inclassables'])
                            for m in methods}

                q_row = [theme, question.qid, question.text, question.nb_categories, question.nb_subcategories]

                q_row.extend([question.nb_total_responses, question.gold[TOTAL]])

                q_row.append(question.nb_empty_responses)
                q_row.append('{:.2f}'.format(question.nb_empty_responses * 100 /question.nb_total_responses))
                q_row.append(question.gold[NON_REPONSES])

                nb_words = [len(r.split()) for r in question.responses.values()]
                q_row.extend(['{:.2f}'.format(x) for x in (np.mean(nb_words), np.std(nb_words), np.median(nb_words))])

                differences = {m: [] for m in methods}
                for category in question.categories.values():
                    c_row = [theme, question.qid, category.id, category.text, len(category.subcategories),
                             question.gold[category.id]]
                    for m in methods:
                        n = clusters[m].get_nb_responses_per_categories(category.id)
                        result = n / question.nb_total_responses
                        c_row.append('{:.2f}'.format(result*100))
                        differences[m].append(abs(result - question.gold[category.id]/100))

                    csv_categories.writerow(c_row)
                q_row.extend(['{:.2f}'.format(np.mean(differences[m])) for m in methods])
                csv_questions.writerow(q_row)

    ## EXP02: comparaison des méthodes entre elles
    # Maintenant, on crée les clusters pour que les effectifs correspondent aux résultats annoncés dans la synthèse
    # et on mesure l'agréement entre les clusters obtenus avec les différentes méthodes
    # A partir de ces scores, on génère une heatmap
    clusters = {theme:
                    {question.qid:
                         {m: gdn.clustering.Clusters.create_clusters(distances[theme][question.qid][m],
                                                                     thresholds[theme][question.qid][m]['gold'])
                          for m in methods}
                     for question in data[theme].questions}
                for theme in data}

    scores = {(theme, q.qid): gdn.clustering.Clusters.agreement_scores(clusters[theme][q.qid], flatten=True)
              for theme in data for q in data[theme].questions}
    df = pd.DataFrame.from_dict(scores).transpose()

    fig = plt.figure(figsize=(10, 20))
    ax = plt.axes()

    cax = ax.matshow(df)
    fig.colorbar(cax, ax=ax)

    ax.set_xticks(np.arange(0, len(df.columns), 1))
    ax.set_xticklabels(df.columns)
    plt.xticks(rotation=90)

    ax.set_yticks(np.arange(0, sum([len(data[theme].questions) for theme in data]), 1))
    ax.set_yticklabels(
        [q.text + ' - ' + themes[theme] + ' ' + q.qid for theme in data for q in data[theme].questions])
    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom + 0.5, top - 0.5)

    plt.savefig('output/02_heatmap_agreement_methods_2.png')


def get_distances_and_thresholds(data):
    distances = {}
    thresholds = {}

    for theme in data:
        distances[theme] = {}
        thresholds[theme] = {}

        progressbar = tqdm.tqdm(data[theme].questions, desc="Chargement/Calcul des distances")
        for question in progressbar:
            thr_filename = os.path.join(PATH_TO_DISTANCES, theme, 'question_{}'.format(question.qid), 'thresholds.json')
            if os.path.exists(thr_filename):
                with open(thr_filename) as f:
                    thresholds[theme][question.qid] = json.load(f)
            else:
                thresholds[theme][question.qid] = {}

            distances[theme][question.qid] = {}
            for method in methods:
                dist_filename = os.path.join(PATH_TO_DISTANCES, theme, 'question_{}'.format(question.qid),
                                             method + '.csv')
                if os.path.exists(dist_filename):
                    progressbar.desc = "Chargement des distances avec {} pour la question {}".format(method, question.qid)
                    distances[theme][question.qid][method] = pd.read_csv(dist_filename, sep=';', header=0, index_col=0)
                else:
                    progressbar.desc = "Calcul des distances avec {} pour la question {}".format(method, question.qid)
                    os.makedirs(os.path.join(PATH_TO_DISTANCES, theme, 'question_{}'.format(question.qid)),
                                             exist_ok=True)
                    if method == 'tfidf':
                        vectorizer = TfIdfVectorizer(question, stop_words)
                        vectorizer.fit()
                    else:
                        method_name, model_name = method.split('_')
                        if model_name in loaded_models:
                            model = loaded_models[model_name]
                        else:
                            model = load_model(model_name)
                            loaded_models[model_name] = model

                        if method_name == 'we':
                            vectorizer = WEVectorizer(question, model, stop_words)
                        elif method_name == 'wwe':
                            vectorizer = WWEVectorizer(question, model, stop_words)

                    vectorizer.fit()

                    distances[theme][question.qid][method] = vectorizer.get_distances()
                    thresholds[theme][question.qid][method] = vectorizer.get_thresholds()

                    # save distances
                    distances[theme][question.qid][method].to_csv(dist_filename, sep=';', na_rep='NA')

                    # save thresholds
                    with open(thr_filename, mode='w') as f:
                        json.dump(thresholds[theme][question.qid], f, indent=4)
    return distances, thresholds


def load_model(model_to_load):
    if model_to_load == 'mngs2':
        mngs2 = mangoes.Embeddings.load('./models/ppmi_svd_fr_dim500_win2_shift1_cds0.75_eig0/')
        mngs2.matrix = mngs2.matrix.normalize()
        mngs2.name = 'mngs2'
        return mngs2
    if model_to_load == 'mngs5':
        mngs5 = mangoes.Embeddings.load('./models/ppmi_svd_fr_dim500_win5_shift1_cds0.75_eig0/')
        mngs5.matrix = mngs5.matrix.normalize()
        mngs5.name = 'mngs5'
        return mngs5
    if model_to_load == 'fasttext':
        if os.path.exists('./models/fasttext_fr_300/'):
            fasttext = mangoes.Embeddings.load('./models/fasttext_fr_300/')
        else:
            # Chargement des vecteurs au format fasttext et sauvegarde au format mangoes
            # pour faciliter le prochain chargement
            with open('./models/cc.fr.300.vec', 'r', encoding='utf-8', newline='\n', errors='ignore') as f:
                shape = tuple(map(int, f.readline().split()))
                m = np.zeros(shape)
                words = []

                for i, line in enumerate(f):
                    word, *vector = line.split()
                    words.append(word)
                    m[i] = vector
            fasttext = mangoes.Embeddings(mangoes.Vocabulary(words), m)
            fasttext.save('./models/fasttext_fr_300/')
        fasttext.name = 'fasttext'
        return fasttext

    raise NotImplementedError


if __name__ == '__main__':
    main()
