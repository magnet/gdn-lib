if [ ! -d "../GDN_open_questions_data" ]; then
  wget -nc http://chercheurs.lille.inria.fr/magnet/GDN/GDN_open_questions_data.tar.gz
  tar -xzvf GDN_open_questions_data.tar.gz
  mv GDN_open_questions_data ..
fi

mkdir -p models
cd models
if [ ! -d "ppmi_svd_fr_dim500_win2_shift1_cds0.75_eig0" ]; then
  echo "Chargement des embeddings créés avec mangoes ..."
  wget -nc http://chercheurs.lille.inria.fr/magnet/ppmi_svd_fr_dim500_win2_shift1_cds0.75_eig0.tar.gz
  tar -xzvf ppmi_svd_fr_dim500_win2_shift1_cds0.75_eig0.tar.gz
fi
if [ ! -f "cc.fr.300.vec" ]; then
  echo "Chargement des embeddings créé avec fasttext .."
  wget -nc https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.fr.300.vec.gz
  gunzip cc.fr.300.vec.gz
fi
#if [ ! -d "camembert.v0" ]; then
#  echo "Chargement camembert .."
#  wget -nc https://dl.fbaipublicfiles.com/fairseq/models/camembert.v0.tar.gz
#  tar -xzvf camembert.v0.tar.gz
#fi
cd ..

