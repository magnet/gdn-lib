import os
import sys
module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

import csv
import glob
import json

import spacy
import tqdm

from gdn import TOTAL, INCLASSABLES, NON_REPONSES
import gdn.data

ORIGINAL_DATA_PATH = 'data'
PREPARED_DATA_PATH = '../GDN_open_questions_data'


def get_text_from_reponse(reponse):
    if not reponse['formattedValue']:
        return None

    return str(reponse['formattedValue'])


def create_theme_from_raw_data(name, path, preprocess=None):
    if not preprocess:
        preprocess = lambda s: s

    questions_from_csv = _read_questions_from_csv(os.path.join(path, name + '_questions.csv'))
    categories_from_csv = _read_categories_csv(os.path.join(path, name + '_categories.csv'))

    print("Chargement du fichier JSON des contributions ...")
    contributions = _read_contributions_from_json(os.path.join(path, name + '.json'))
    print("...Done")

    print("Pré-traitement des catégories et contributions ...")
    questions = []
    for question in tqdm.tqdm(questions_from_csv, total=len(questions_from_csv), desc="Questions"):
        questions.append(create_question_from_raw_data(question,
                                                       categories_from_csv[question['id']],
                                                       contributions,
                                                       preprocess))

    return gdn.data.Theme(name, questions, len(contributions))


def create_question_from_raw_data(question_info_from_csv, categories_from_csv, contributions,
                                  preprocess=None):
    if not preprocess:
        preprocess = lambda s: s

    # build list of categories and expected scores for each of them in gold
    categories = {}
    gold = {TOTAL: question_info_from_csv[TOTAL]}

    for c in categories_from_csv.values():
        if c['text'] in (INCLASSABLES, NON_REPONSES):
            gold[c['text']] = c['percentage']
        else:
            gold[c['id']] = c['percentage']
            categories[c['id']] = gdn.data.Category(c['id'],
                                                    preprocess(c['text']),
                                                    [preprocess(sc) for sc in c['subcategories']])

    position = question_info_from_csv['position']
    parent_position = question_info_from_csv['parent_position']

    responses = {}

    nb_empty_responses = 0
    nb_predefined_responses = 0

    json_text = contributions[0]['responses'][position]['questionTitle'].strip()
    if json_text != question_info_from_csv['text']:
        print('Libellé différent entre le json et le csv')
        print('JSON:', json_text)
        print(question_info_from_csv)

    if parent_position is None:
        # no filter for this question : we consider all the responses
        for i, contribution in tqdm.tqdm(enumerate(contributions), total=len(contributions),
                                         leave=False, desc='Contributions'):
            # check that each question is at the same position for all contributions
            assert contribution['responses'][position]['questionTitle'].strip() == json_text

            text = get_text_from_reponse(contribution['responses'][position])
            if text:
                responses[i] = preprocess(text)
            else:
                nb_empty_responses += 1

    elif parent_position == position:
        # this question is semi open. We only consider the open responses and count the pre-determined ones
        for i, contribution in tqdm.tqdm(enumerate(contributions), total=len(contributions),
                                         leave=False, desc='Contributions'):
            assert contribution['responses'][position]['questionTitle'].strip() == json_text

            response_from_list = json.loads(contribution['responses'][parent_position]['value'])['labels']

            if not response_from_list:
                text = get_text_from_reponse(contribution['responses'][position])
                if text:
                    responses[i] = preprocess(text)
                else:
                    nb_empty_responses += 1
            else:
                nb_predefined_responses += 1

    else:
        # this question is conditioned by the answer to a previous question. We only consider the contributions where
        # we find this expected value for the parent question
        value = question_info_from_csv['parent_value']
        for i, contribution in tqdm.tqdm(enumerate(contributions), total=len(contributions),
                                         leave=False, desc='Contributions'):
            assert contribution['responses'][position]['questionTitle'].strip() == json_text

            response_to_parent = json.loads(contribution['responses'][parent_position]['value'])['labels']
            if response_to_parent and value in response_to_parent:
                text = get_text_from_reponse(contribution['responses'][position])
                if text:
                    responses[i] = preprocess(text)
                else:
                    nb_empty_responses += 1

    return gdn.data.Question(question_info_from_csv['id'],
                    question_info_from_csv['text'],
                    categories,
                    responses,
                    nb_empty_responses, nb_predefined_responses,
                    gold)


def _read_questions_from_csv(path_to_questions):
    questions_from_csv = []

    with open(path_to_questions) as f:
        for row in csv.DictReader(f, delimiter=';'):
            question = {'id': row['no. question'],
                        'position': int(row['pos. question']) - 1,
                        'text': ' '.join(row['texte question'].split()), # remove double spaces
                        TOTAL: int(row['nb contrib']),
                        'parent_position': int(row['pos. question parente']) - 1 if row['pos. question parente'] else None,
                        'parent_value': row['value'] if row['value'] else None}

            questions_from_csv.append(question)

    return questions_from_csv


def _read_categories_csv(path):
    categories = {}

    with open(path) as f:
        for row in csv.DictReader(f, delimiter=';'):
            question_id = row['no. question']
            if question_id not in categories:
                categories[question_id] = {}

            category_id = row['no. cluster']
            if row['no. sous-cluster'] == '-':
                # main category
                categories[question_id][category_id] = {'question_id': question_id,
                                                        'id': category_id,
                                                        'text': row['description'],
                                                        'percentage': float(row['pourcentage']),
                                                        'subcategories': []}
            else:
                # subcategory
                categories[question_id][category_id]['subcategories'].append(row['description'])

    return categories


def _read_contributions_from_json(path_to_contributions):
    with open(path_to_contributions) as f:
        try:
            return json.load(f)
        except json.JSONDecodeError:
            # il manque un crochet fermant à la fin des fichiers téléchargés sur le site du grand débat
            f.seek(0)
            lines = f.readlines()
            lines[-1] = lines[-1] + ']'
            return json.loads('\n'.join(lines))


def save_theme(theme):
    for question in theme.questions:
        question_folder_path = os.path.join(PREPARED_DATA_PATH, theme.name, 'question_' + question.qid)
        save_question(question, question_folder_path)

    with open(os.path.join(PREPARED_DATA_PATH, theme.name, 'nb_contributions.txt'), 'w') as f:
        f.write('{}'.format(theme.nb_contributions))


def save_question(question, path):
    """Save the question """
    os.makedirs(path, exist_ok=True)

    with open(path + '/info.json', 'w') as f:
        json.dump({
            'qid': question.qid,
            'text': question.text,
            'nb_empty_responses': question.nb_empty_responses,
            'nb_predefined_responses': question.nb_predefined_responses,
            'gold': question.gold
        }, f, indent=True)

    with open(path + '/categories.csv', 'w') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(['question_id', 'id', 'text', 'subcategories'])
        for category in question.categories.values():
            writer.writerow([question.qid, category.id, category.text, '|'.join(category.subcategories)])

    with open(path + '/reponses.csv', 'w') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(('position', 'text'))
        for response in question.responses.items():
            writer.writerow(response)


def main():
    nlp = spacy.load("fr_core_news_sm")
    tokenizer = spacy.lang.fr.French().Defaults.create_tokenizer(nlp)

    def tokenize_and_lower(s):
        if not s:
            return s
        s = s.strip()
        s = s.replace('\x00', ' ')  # remove NUL character

        if not s:
            return s
        return (' '.join([t.text for t in tokenizer(s)])).lower()

    for file_name in glob.glob(ORIGINAL_DATA_PATH + '/*.json'):
        name = os.path.basename(file_name)[:-5]
        print(name)
        theme = create_theme_from_raw_data(name, ORIGINAL_DATA_PATH, preprocess=tokenize_and_lower)
        save_theme(theme)


if __name__ == '__main__':
    main()
