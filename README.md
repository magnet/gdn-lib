# gdn-lib

Ce projet met à disposition :
- un [dataset construit à partir des réponses aux questions ouvertes du grand débat](http://chercheurs.lille.inria.fr/magnet/GDN/GDN_open_questions_data.tar.gz) et le script utilisé pour créer ce dataset
- une librairie (Python 3) avec des classes et fonctions de bases pour manipuler ce dataset
- le script utilisé pour obtenir les résultats présentés lors des [JE GJ DN](https://metsem.hypotheses.org/765) 
dans `scripts/metsem`

## Pré-requis

Les librairies nécessaires à l'utilisation de l'ensemble du code python fourni dans ce projet sont
listées dans le fichier `requirements.txt` qui peut être utilisé : 

- soit pour créer un environnement conda (recommandé) :

```
conda create --name gdn python==3.7.6
conda activate gdn
conda install --file requirements.txt 
pip install --no-deps mangoes
```

- soit pour installer les modules requis avec pip :

```
pip install -r requirements.txt
pip install --no-deps mangoes
```

## Dataset : réponses aux questions ouvertes

Le dataset a été construit à partir des fichiers de contributions téléchargeables sur cette page :
    
    https://granddebat.fr/pages/donnees-ouvertes
    
(versions JSON du 21/03/2019)

et des rapports de synthèses de contributions en ligne disponibles sur cette page :
    
    https://granddebat.fr/pages/syntheses-du-grand-debat
    
(Premières versions)  
Les données issues de ces rapports - notamment, les intitulés des catégories proposées pour classer les réponses et 
les résultats (pourcentage) associés - ont été extraites dans des fichiers csv (voir dans `scripts/dataset_creation/data`)

Le dataset est disponible au téléchargement : http://chercheurs.lille.inria.fr/magnet/GDN/GDN_open_questions_data.tar.gz  
Il peut être reconstruit en utilisant le script `scripts/dataset_creation/run.sh`


## Librairie

```python
>>> import gdn.data
>>> path_to_dataset = 'scripts/GDN_open_questions_data'
>>> gdn_dataset = gdn.data.load_dataset(path_to_dataset)
>>> fdp = gdn_dataset['LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES']
>>> fdp_q1 = fdp.questions[0]
```

La librairie repose sur 3 classes de bases définie dans le module `gdn.data`:

### Theme
La classe `Theme` permet de représenter un des 4 grands thèmes proposés.

Propriétés :
- `name`
- `questions` : liste d'objets de la classe Question qui représentent les questions **ouvertes** posées sur ce thème
- `nb_questions` : nombre de questions, ouvertes ou semi-ouvertes, disponibles pour ce thème dans le dataset
- `nb_contributions` : le nb de contributions créées par les utilisateurs du site pour ce thème 

### Question
La classe `Question` est la classe principale. Elle permet de regrouper les informations concernant une des questions 
**ouverte** posée sur le site du grand débat

Propriétés :
- `qid`: Identifiant donné à la question (suivant la numérotation utilisée dans les synthèses)
- `text`: Intitulé de la question
- `categories`: Catégories proposées pour classer les réponses à cette question sous la forme 
(dictionnaire avec clé = identifiant, valeur = Category)  
- `nb_categories`
- `nb_subcategories`: nombre total de sous-catégories (pour l'ensemble des catégories)  
- `responses`: réponses non vides (dictionnaire avec clé = position de la réponse dans le JSON original, valeur = texte
de la réponse)
- `nb_empty_responses`: nombre de contributions où la réponse à cette question était vide
- `nb_predefined_responses`: pour les question semi-ouvertes, nb de contributions où la réponse à cette question 
était une des réponses prédéfinies
- `nb_total_responses`: nombre de réponses attendues pour cette question (pour la plupart des questions, c'est le nombre 
de contributions dans le thème. Mais si la question était conditionnée par la réponse à une question précédente, c'est 
le nombre de contributions avec cette réponse attendue )
- `gold`: les résultats extraits de la synthèse i.e. le pourcentage de réponses affectées à chaque catégorie (dict où 
clé = id de la catégorie, valeur=pourcentage)


Méthode :
- `search_in_responses(pattern)` : recherche les réponses qui contiennent le pattern demandé (accepte les expressions 
régulières )

### Categorie
Propriétés:
- `id` : numéro de la catégorie dans la synthèse 
- `text` : libellé de la catégorie 
- `subcategories` : libellés des sous-catégories de cette catégorie sous forme de liste
